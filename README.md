# OS - oblig 2 #

Creating various linux bash scripts

Done by: Nataniel Gåsøy

## myprocinfo.bash ##

Lag et script myprocinfo.bash som gir brukeren følgende meny med tilhørende
funksjonalitet:
1 - Hvem er jeg og hva er navnet p˚a dette scriptet?
2 - Hvor lenge er det siden siste boot?
3 - Hvor mange prosesser og tr˚ader finnes?
4 - Hvor mange context switch'er fant sted siste sekund?
5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode
og i usermode siste sekund?
6 - Hvor mange interrupts fant sted siste sekund?
9 - Avslutt dette scriptet
Velg en funksjon:

## chromemajPF.bash ##

Anta at nettleseren chrome kjører. Skriv et bash script chromemajPF.bash
som skriver ut antall major page faults hver av chrome sine prosesser har forarsaket. ˚
I tillegg skal scriptet skrive ut en melding om en av chrome sine prosesser har
forarsaket mer enn 1000 major page faults.

## fsinfo.bash ##
Skriv et script fsinfo.bash som tar en directory som argument og skriver ut
• Hvor stor del av partisjonen directorien befinner seg pa som er full ˚
• Hvor mange filer finnes i directorien (inkl subdirectorier), gjennomsnittlig
filstørrelse, og full path til den største filen
• Hvilken fil (IKKE ta med directories) har flest hardlinker til seg selv

### Shellcheck ###

sudo apt install shellcheck

chromemajPF.bash:
	- "Double quote to prevent globbing and word splitting." for variables (pageFaults=$(ps --no-headers -o maj_flt $i)).	(added double quotes around variables).

fsinfo.bash:
	- "mostHardlinksNr appears unused. Verify it or export it." 	(It appears to not be in use, so i deleted it).
	- "Double quote to prevent globbing and word splitting."	(added double quotes around variables).

myprocinfo.bash:
	- "Tips depend on target shell and yours is unknown. Add a shebang." for !/ bin / bash 		(removed the spaces).
	- "Double quote to prevent globbing and word splitting."					(added double quotes around variables).



