#!/bin/bash
#chromemajPF.bash

for i in $(pgrep chrome); do
    pageFaults=$(ps --no-headers -o maj_flt "$i")
    if [ "$pageFaults" -gt 1000 ]
    then
        echo "Chrome $i has caused $pageFaults major page faults (more than 1000!!!)"
    else
        echo "Chrome $i has caused $pageFaults major page faults"
    fi
done
