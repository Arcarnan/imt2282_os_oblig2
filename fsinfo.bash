#!/bin/bash
# fsinfo.bash

directory="$1"                #first argument after function call is the directory, i.e ~/imt2282_operativeSystem
fullPercentage=$(df -k "$directory" | awk 'NR==2{print $5}')
numberOfFiles=$(find "$directory" -type f | wc -l)
sizeOfDirectory=$(du "$1" --summarize | awk '{print $1}')
averageFileSizeByte=$(awk "BEGIN {print $sizeOfDirectory/$numberOfFiles}")
largestFileSizeByte=$(find "$directory" -printf '%s %p\n'|sort -nr|head -n 1 | awk '{print $1}')
largestFileName=$(find "$directory" -printf '%s %p\n'|sort -nr|head -n 1|cut -f2- -d ' ')

echo -e "\n\tThe partition $directory is located in is $fullPercentage full."
echo -e "\tIt has $numberOfFiles files in it."
echo -e "\tThe largest file is $largestFileName, and is $largestFileSizeByte Bytes ($(numfmt --to=iec-i --suffix=B "$largestFileSizeByte")). "
echo -e "\tThe average filesize for files in this folder is $averageFileSizeByte Bytes. " #$(($averageFileSizeByte/1024))Mib.

mostHardlinks=0
nr=1
for i in $(find "$1" -type f -ls | awk '{print $4}'); do
    if [[ "$i" -gt "$mostHardlinks" ]]; then
        mostHardlinks="$i"
    fi
    ((nr++))
done

fileName=$(find "$1" -type f -ls | sort | awk 'END{print $11}')
echo -e "\tThe file with most hard links is $fileName. It has $mostHardlinks hard link(s). \n"
